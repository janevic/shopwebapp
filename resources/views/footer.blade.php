<footer class="footer" >
    <div class="panel-footer" style="background-color: #090909; overflow: auto; color: white">
        <h3>Shop app</h3>
        <p>Contact info:</p>
        <ul class="list-group col-lg-2">
            <li class="list-group-item" style="background-color: #090909">
                Phone: {{ $phone }}
            </li>
            <li class="list-group-item" style="background-color: #090909">
                Mail: {{ $mail }}
            </li>
        </ul>
    </div>
</footer>