@extends('app')

@section('content')
    <div class="container">
        <div class="panel-heading col-md-12 col-sm-12 text-center">
            <h2>This place is reserved for banners or name of the shop</h2>
        </div>
        <div class="panel-body col-md-12 col-sm-12">
            <div class="list-group col-md-2 col-sm-12">
                @if($category_id == "null")
                    <a href="{{ url('/') }}" class="list-group-item active">All Products</a>
                @else
                    <a href="{{ url('/') }}" class="list-group-item">All Products</a>
                @endif
                @foreach($categories as $category)
                    @if($category_id == $category->id)
                        <a href="{{ url('/'.$category->id) }}" class="list-group-item active">
                            {{ $category->name }}
                        </a>
                    @else
                        <a href="{{ url('/'.$category->id) }}" class="list-group-item">
                            {{ $category->name }}
                        </a>
                    @endif
                @endforeach
            </div>
            <div class="col-md-8 col-sm-8">
                @if(count($products) > 0)
                    @foreach($products as $product)
                        <h3>{{ $product->name }}</h3>
                        <p>{{ $product->description }}</p>
                        <p>Price: {{ $product->price }}</p>
                        <p>Products left: {{ $product->quantity }}</p>
                    @endforeach
                @else
                    There are no products in this category
                @endif
            </div>

        </div>
        <!--
        <div class="panel-footer col-md-12 col-sm-12">
            Here goes the TM staff the location and other info about the shop
        </div>
        -->
    </div>
@endsection