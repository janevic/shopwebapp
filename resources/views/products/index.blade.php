@extends('app')

@section('content')
    <div class="panel-heading">
        <h2>Added products</h2>
    </div>
    <div class="panel-body">
        @if(count($products) > 0)
        @foreach($products as $product)
            <div class="panel well">
                <h3><a href="{{ url('/product/' . $product->id) }}">{{ $product->name }}</a></h3>
                <p>{{ $product->description }}</p>
                <form class="form-horizontal" method="POST" action="{{ '/product/'. $product->id }}">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        @endforeach
        @endif
        <a href="/product/create" class="btn btn-primary">New Product</a>
    </div>

@endsection