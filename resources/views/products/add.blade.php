@extends('app')

@section('content')
    <div class="panel-heading">
        <h2>Add products</h2>
    </div>
    <div class="panel panel-body">
        <!--<form class="form-horizontal panel-body" enctype="multipart/form-data" method="POST" action="{{ url('/product') }}">

            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="nameLbl" for="name">Name: </label>
                <input class="col-sm-6 col-md-4" name="name" id="name" type="text" />
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="descriptionLbl" for="description">Description: </label>
                <input class="col-sm-6 col-md-4" name="description" id="description" type="text" />
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="quantityLbl" for="quantity">Quantity: </label>
                <input class="col-sm-6 col-md-4" name="quantity" id="quantity" type="number" />
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="priceLbl" for="price">Price: </label>
                <input class="col-sm-6 col-md-4" name="price" id="price" type="number" />
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="categoryLbl" for="category">Category: </label>
                <select class="col-sm-6 col-md-4" name="category" id="category">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-2" for="imgPath">Select image:</label>
                <input type="image" class="col-sm-3 col-md-2" id="imgPath" name="imgPath" />
            </div>
            <button class="btn btn-primary" type="submit" >Save new product</button>
        </form>-->

        {!! Form::open(array('url' => '/product','enctype' => 'multipart/form-data', 'method' => 'POST')) !!}
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="col-sm-3 col-md-2" id="nameLbl" for="name">Name: </label>
            <input class="col-sm-6 col-md-4" name="name" id="name" type="text" />
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2" id="descriptionLbl" for="description">Description: </label>
            <input class="col-sm-6 col-md-4" name="description" id="description" type="text" />
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2" id="quantityLbl" for="quantity">Quantity: </label>
            <input class="col-sm-6 col-md-4" name="quantity" id="quantity" type="number" />
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2" id="priceLbl" for="price">Price: </label>
            <input class="col-sm-6 col-md-4" name="price" id="price" type="number" />
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2" id="categoryLbl" for="category">Category: </label>
            <select class="col-sm-6 col-md-4" name="category" id="category">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-2" for="imgPath">Select image:</label>
            {!! Form::file('imgPath', array('class' => 'imgPath')) !!}
        </div>
        <button class="btn btn-primary" type="submit" >Save new product</button>
        {!! Form::close() !!}
    </div>
@endsection