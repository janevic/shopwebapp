@extends('app')

@section('content')
<div class="container">
    <h3><a href="{{ url('/product') }}">{{ $product->name }}</a></h3>
    <p>Product description: {{ $product->description }}</p>
    <p>Products left: {{ $product->quantity }}</p>
    <p>Price of a single product: {{ $product->price }}</p>
    <p>Price of all the products that are left is: {{ $product->quantity * $product->price }}</p>
</div>
@endsection