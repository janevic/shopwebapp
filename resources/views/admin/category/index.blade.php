@extends('app')

@section('content')
    <div class="panel-heading">
        <h2>Categories</h2>
    </div>
    <div class="panel-body">
        @foreach($categories as $category)
            <div class="panel well">
                <h3>{{ $category->name }}</h3>
                <p>{{ $category->description }}</p>
                <form class="form-horizontal" method="POST" action="{{ '/category/'. $category->id }}">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        @endforeach
        <a href="/category/create" class="btn btn-primary">New Category</a>
    </div>

@endsection