@extends('app')

@section('content')
    <div class="panel-heading">
        <h2>Add products</h2>
    </div>
    <div class="panel panel-body">
        <form class="form-horizontal panel-body" method="POST" action="{{ url('/categories') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="name" for="name">Name: </label>
                <input class="col-sm-6 col-md-4" name="name" id="name" type="text" />
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-2" id="description" for="description">Description: </label>
                <input class="col-sm-6 col-md-4" name="description" id="description" type="text" />
            </div>
            <button class="btn btn-primary" type="submit">Save new category</button>
        </form>
    </div>
@endsection