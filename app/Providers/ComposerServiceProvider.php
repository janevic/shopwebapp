<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 3/3/2017
 * Time: 2:25 PM
 */

namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot() {
        View::composer('app', 'App\Http\ViewComposers\FooterComposer');
    }

    public function register() {
        //
        $this->app->bind('App\Http\Repositories\ComposerServiceProvider');
    }
}