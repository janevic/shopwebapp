<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpKernel\Tests\HttpCache\HttpCacheTest;
use App\Http\Repositories;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Http\Interfaces\IProduct', 'App\Http\Repositories\ProductRepository');
        $this->app->bind('App\Http\Interfaces\ICategory', 'App\Http\Repositories\CategoryRepository');
        $this->app->bind('App\Http\Interfaces\IContactInfo', 'App\Http\Repositories\ContactInfoRepository');
    }
}
