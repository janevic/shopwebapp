<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\ICategory;
use App\Http\Interfaces\IProduct;
use App\Http\Requests;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(IProduct $product, ICategory $category) {

        $categories = $category->getAllCategories();

        $products = $product->getAllProducts();

        return view('public.home', [
            'categories' => $categories,
            'products' => $products,
            'category_id' => "null"
        ]);
    }

    public function categoryDetails($category_id, IProduct $product, ICategory $category) {

        $categories = $category->getAllCategories();

        $productsByCategory = $product->getProductsByCategory((int)$category_id);

        return view('public.home', [
            'categories' => $categories,
            'products' => $productsByCategory,
            'category_id' => $category_id
        ]);
    }

}
