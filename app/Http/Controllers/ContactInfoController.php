<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 3/3/2017
 * Time: 2:29 PM
 */

namespace App\Http\Controllers;

use App\Http\Interfaces\IContactInfo;

class ContactInfoController
{
    protected $contactInfo;

    public function __construct(IContactInfo $contactInfo) {
        $this->contactInfo = $contactInfo;
    }
}