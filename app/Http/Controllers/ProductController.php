<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\ICategory;
use App\Http\Interfaces\IProduct;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    //
    protected $products;

    public function __construct(IProduct $product) {
        $this->middleware('auth')->except('GET');

        $this->products = $product;
    }

    public function index(Request $request)
    {
        return view('products.index', [
            'products' => $this->products->forUser($request->user()),
        ]);
    }

    public function create(ICategory $category) {
        return view('products.add', [
            'categories' => $category->getAllCategories()
        ]);
    }

    public function store(IProduct $productRepository, Request $request) {
        $productRepository->saveProduct($request);

        return redirect('/product');
    }

    public function destroy(Request $request, Product $product) {
        $this->authorize('destroy', $product);

        $product->delete();

        return redirect('/product');
    }

    public function details(Product $product) {
        return view('products.details', [
            'product' => $product
        ]);
    }

}
