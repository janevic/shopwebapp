<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Interfaces\ICategory;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    //
    protected $category;

    public function __construct(ICategory $category) {
        $this->middleware('auth');

        $this->category = $category;
    }

    public function index(Request $request) {

        $categories = $this->category->getAllCategories();

        return view('admin.category.index', [
            'categories' => $categories
        ]);
    }

    public function create() {
        return view('admin.category.add');
    }

    public function store(Request $request) {
        $this->category->saveCategory($request);

        return redirect('/categories');
    }

    public function destroy(Category $category) {
        $this->authorize('destroy', $category);

        $category->delete();

        return redirect('/categories');
    }
}
