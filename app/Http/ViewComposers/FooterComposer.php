<?php

/**
 * Created by PhpStorm.
 * User: Darko
 * Date: 03-Mar-17
 * Time: 9:26 PM
 */
namespace App\Http\ViewComposers;

use App\ContactInfo;
use App\Http\Interfaces\IContactInfo;
use App\Http\Repositories\ContactInfoRepository;
use Illuminate\Contracts\View\View;

class FooterComposer
{
    /*protected $contactInfo;

    public function __construct(ContactInfoRepository $contactInfo)
    {
        $this->contactInfo = $contactInfo;
    }*/

    public function compose(View $view)
    {
        $data = json_decode(ContactInfo::all()->first());
        if($data != null) {
            $view->with('phone', $data->phone);
            $view->with('mail', $data->mail);
        } else {
            $view->with('phone', "");
            $view->with('mail', "");
        }
    }
}