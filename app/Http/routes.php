<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
View::composer('app', function($view)
{
    $view->with('test', 'This is the testValue');
});

Route::auth();

Route::get('/', 'PublicController@index');

Route::get('/product', 'ProductController@index');
Route::get('/product/create', 'ProductController@create');
Route::get('/product/{product}', 'ProductController@details');
Route::post('/product', 'ProductController@store');
Route::delete('/product/{product}', 'ProductController@destroy');

Route::get('/categories', 'CategoryController@index');
Route::get('/category/create', 'CategoryController@create');
Route::post('/categories', 'CategoryController@store');
Route::delete('category/{category}', 'CategoryController@destroy');

Route::get('/{category}', 'PublicController@categoryDetails');