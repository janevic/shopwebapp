<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 12/26/2016
 * Time: 3:05 PM
 */

namespace App\Http\Interfaces;


use Illuminate\Http\Request;

interface ICategory
{
    public function getAllCategories();

    public function getCategoryById($categoryId);

    public function getCategoryByName($categoryName);

    public function saveCategory(Request $request);
}