<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 3/3/2017
 * Time: 2:31 PM
 */

namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface IContactInfo
{
    public function getAllContactInfo();

    public function saveContactInfo(Request $request);
}