<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 12/26/2016
 * Time: 2:08 PM
 */

namespace App\Http\Interfaces;
use App\User;
use Illuminate\Http\Request;

interface IProduct
{
    public function forUser(User $user);

    public function getAllProducts();

    public function getProduct($product_id);

    public function saveProduct(Request $request);

    public function getProductsByCategory($category_id);
}