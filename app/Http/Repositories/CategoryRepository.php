<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 12/26/2016
 * Time: 12:26 PM
 */

namespace App\Http\Repositories;


use App\Category;
use App\Http\Interfaces\ICategory;
use Illuminate\Http\Request;

class CategoryRepository implements ICategory
{
    protected $category;

    public function __construct(Category $category) {
        $this->category = $category;
    }

    public function getAllCategories() {
        return $this->category->all();
    }

    public function getCategoryById($id) {
        return $this->category->all()->where('id', $id);
    }

    public function getCategoryByName($category_name) {
        return $this->category->all()->where('name', $category_name);
    }

    public function saveCategory(Request $request) {
        $this->category->create([
            'name' => $request->name,
            'description' => $request->description
        ]);
    }



}