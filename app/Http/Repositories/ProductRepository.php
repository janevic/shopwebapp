<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 12/23/2016
 * Time: 6:32 PM
 */

namespace App\Http\Repositories;

use App\Http\Interfaces\IProduct;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Storage;
use Image;
use Symfony\Component\Console\Output\ConsoleOutput;


class ProductRepository implements IProduct
{
    protected $product;

    public function __construct(Product $product) {
        $this->product = $product;
    }

    public function forUser(User $user) {
        return $user->products()
            ->orderBy('created_at', 'asc')
            ->get();
    }

    public function getAllProducts() {
        return $this->product->all();
    }

    public function getProduct($product_id) {
        return $this->product->all()->where('id', 4);
    }

    public function saveProduct(Request $request) {
        $output = new ConsoleOutput();
        //$output->writeln('imgPath: ' . $request->file('imgPath')->getRealPath());

        $img = Image::make($request->file('imgPath'));
        $names = explode('\\', $request->file('imgPath'));
        $name = $names[count($names)-1];
        $name = explode('.', $name)[0] . ".jpeg";
        //$output->writeln('name: ' . var_dump($img));
        Storage::disk('public')->put($name, $img);
        //Storage::put('file.jpg', $request->imgPath, 'public');
        //$img->save($request->imgPath.".png");

        $request->user()->products()->create([
            'category_id' => $request->category,
            'name' => $request->name,
            'description' => $request->description,
            'quantity' => $request->quantity,
            'price' => $request->price
        ]);
    }

    public function updateProduct(Request $request) {
        $product = $this->getProduct($request->id);
        if($product != null) {
            $request->user()->products()->update([
                'category_id' => $request->category,
                'name' => $request->name,
                'description' => $request->description,
                'quantity' => $request->quantity,
                'price' => $request->price
            ]);
        }
    }

    public function getProductsByCategory($category_id) {
        $products = $this->product->all()->where('category_id', $category_id);
        return $products;
    }
}