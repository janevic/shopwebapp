<?php
/**
 * Created by PhpStorm.
 * User: Bobi
 * Date: 3/3/2017
 * Time: 2:33 PM
 */

namespace App\Http\Repositories;


use App\ContactInfo;
use App\Http\Interfaces\IContactInfo;
use Illuminate\Http\Request;

class ContactInfoRepository implements IContactInfo
{
    protected $contactInfo;

    public function __construct(IContactInfo $contactInfo) {
        $this->contactInfo = $contactInfo;
    }

    public function getAllContactInfo() {
        return $this->contactInfo->all();
    }

    public function saveContactInfo(Request $request) {
        $this->contactInfo->create([
            'mail' => $request->mail,
            'phone' => $request->phone
        ]);
    }
}